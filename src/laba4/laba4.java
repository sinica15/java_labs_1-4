package laba4;

public class laba4 {

    public static void main (String[] args){

        Employee[] employees = new Employee[5];

        employees[0] = new Employee("vasia", "22.06.99", 6899);
        employees[1] = new Employee("petia", "12.02.85", 9549);
        employees[2] = new Employee("jeka", "09.07.67", 9449);
        employees[3] = new Chief("sania", "08.02.77", 5599, 1.6);
        employees[4] = new Chief("max", "12.12.45", 2649, 0.9);

        getEmplInfo(employees);
        allSalaryIncrease(employees, 1.5);
        getEmplInfo(employees);

    }

    public static void allSalaryIncrease (Employee[] arr ,double percent){
        for (int i = 0; i < arr.length; i++){
            arr[i].salaryIncrease(percent);
        }
    }

    public static void getEmplInfo (Employee[] arr){
        System.out.println("====== employees ======");
        for (int i = 0; i < arr.length; i++){
            System.out.print(arr[i].getName());
            System.out.print("  ");
            System.out.print(arr[i].getBirthday());
            System.out.print("  ");
            System.out.print(arr[i].getSalary());
            System.out.println();
        }
        System.out.println("=======================");
    }
}
