package laba4;

public class Employee{
    String name;
    String birthday;
    double salary;

    Employee(String name, String birthday, double salary){
        this.name = name;
        this.birthday = birthday;
        this.salary = salary;
    }

    void salaryIncrease(double percent){
        this.salary = this.salary * percent;
    }

    String getName(){
        return name;
    }
    String getBirthday(){
        return birthday;
    }
    double getSalary(){
        return salary;
    }
}