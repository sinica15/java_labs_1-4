package laba4;

public class Chief extends Employee{
    private double boostFactor;

    Chief(String name, String birthday, double salary, double boostFactor){
        super(name, birthday, salary);
        this.boostFactor = boostFactor;
    }

    @Override
    void salaryIncrease(double percent){
        this.salary = this.salary * percent * this.boostFactor;
    }

}
