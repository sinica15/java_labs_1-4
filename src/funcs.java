import java.util.Random;

/**
 * Created by ruja_ on 23.03.2019.
 */
final public class funcs {

    static void randArrGen(int[] arr){
        Random rnd = new Random (System.currentTimeMillis());
        int defaultMin = 1;
        int defaultMax = 50;
        for (int i = 0; i < arr.length; i++){
            arr[i] = defaultMin + rnd.nextInt(defaultMax - defaultMin + 1);

        }
    }

    static void printArr(int[] arr){
        System.out.print("< ");
        for (int i = 0; i < arr.length - 1; i++){
            System.out.print(arr[i] + " ");
        }
        System.out.println(">");

    }
}
