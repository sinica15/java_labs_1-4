import java.util.Random;


public class laba2 {
    public static void main (String[] args){
//        Random rnd = new Random (System.currentTimeMillis());
//        int randNumb = min + rnd.nextInt(max - min + 1);

        int array[] = new int[20];
        funcs.randArrGen(array);
        funcs.printArr(array);
        sortRecurs(array);
        funcs.printArr(array);

        funcs.randArrGen(array);
        funcs.printArr(array);
        sortSelect(array);
        funcs.printArr(array);



    }

    static void sortRecurs(int[] arr){
        sortRecurs(arr, 0, arr.length - 1);
    }

    static void sortRecurs(int[] arr, int first, int last){
        int supporting = (first + last) / 2;
        for (int i = first; i < supporting;){
            if (arr[i] >= arr[supporting]){
                supporting = insertRight(arr, supporting, i);
            }else {
                i++;
            }
        }
        for (int i = supporting + 1; i < last; ){
            if (arr[i] < arr[supporting]){
                supporting = insertLeft(arr, supporting, i);
            }else {
                i++;
            }
        }
        if(last - first > 3){
            sortRecurs(arr, first, supporting);
            sortRecurs(arr, supporting + 1, last);
        }
    }

    static int insertLeft(int[] arr, int supInd, int inInd){
        int ind = arr[inInd];
        for (int i = inInd; i > supInd; i--){
            arr[i] = arr[i - 1];
        }
        arr[supInd] = ind;

        supInd++;
        return supInd;
    }

    static int insertRight(int[] arr, int supInd, int inInd){
        int ind = arr[inInd];
        for (int i = inInd; i < supInd; i++){
            arr[i] = arr[i + 1];
        }
        arr[supInd] = ind;
        supInd--;
        return supInd;
    }

    static void sortSelect(int[] arr){
        for (int i = 0; i < arr.length-1; i++){
            int minIndex = searchMinInRange(arr, i);
            int temp = arr[i];
            arr[i] = arr[minIndex];
            arr[minIndex] = temp;
        }
    }

    static int searchMinInRange(int[] arr, int searchStart){
        int indexMinEl=searchStart;
        for (int i = searchStart; i < arr.length; i++){
            if(arr[i]<arr[indexMinEl]){indexMinEl = i;}
        }
        return indexMinEl;
    }


}
